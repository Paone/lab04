import org.gradle.api.Plugin
import org.gradle.api.Project

class GreetingPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        // Add the 'greeting' extension object
        project.extensions.create("greeting", GreetingPluginExtension)
        // Append a closure {} to task hello
        project.task('hello') << {
            println 'DUMMY_ENV is: ' + System.getenv()['DUMMY_ENV']
            project.exec {
              environment 'DUMMY_ENV', project.greeting.message
              commandLine 'env'
            }
            project.exec {
              commandLine 'echo', 'Just saying: ', project.greeting.message
            }

        }
    }
}

class GreetingPluginExtension {
    def String message = 'Hello from GreetingPlugin'
}
